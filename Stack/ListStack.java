package ds;

import java.util.ArrayList;
import java.util.List;

public class ListStack<X>
{
	private List data;
	private int stackPointer;
	public ListStack()
	{
		data = new ArrayList<X>();
		stackPointer = 0;
	}
	public void push(X newItem)
	{
		stackPointer++;
		data.add(newItem);
	}
	public X pop()
	{
		if (stackPointer == 0) {
			throw new IllegalStateException("No more items on the stack");
		}
		return (X) data.remove(--stackPointer);
	}
	public boolean contains(X item)
	{
		boolean found = false;
		
		found = data.contains(item);
		
		return found;
	}
	public X access(X item) {
		int index = data.indexOf(item);
		if(index == -1) {
			throw new IllegalStateException("There's no element in the array");
		}
		return (X) data.get(index);
	}
	public int size()
	{
		return data.size();
	}
}
